/**
 * Created with JetBrains PhpStorm.
 * User: Jan
 * Date: 9.3.13
 * Time: 19:23
 * To change this template use File | Settings | File Templates.
 */

module Mapping {
    export class BasicMap {

        public map:google.maps.Map;
        public markers:google.maps.Marker[] = [];
        public paths:google.maps.Polyline[] = [];

        private loaded = false;

        constructor(private mapElement:Element, private options) {
            $(window).load(()=> {
                this.initialize()
            });

        }

        public initialize() {
            var mapOptions = {
                zoom: this.options.zoom,
                center: new google.maps.LatLng(this.options.center.lat, this.options.center.lng),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDoubleClickZoom: true,
                streetViewControl: false
            }

            this.map = new google.maps.Map(this.mapElement, mapOptions);
            this.map.setTilt(0);

            this.loaded = true;
        }
        public loadVenues(venues) {
            if (venues) {
                $.each(venues, (index, item)=>{
                    var marker = this.createMarker({
                        position: new google.maps.LatLng(item.location.lat, item.location.lng),
                        title: item.name,
                        icon: (item.wikipedia == null?'/img/red.png':undefined)
                    });
                    this.markers.push(marker);
                    if(item.wikipedia != null) {
                        google.maps.event.addListener(marker,"click", function() => {
                            _this.markerClicked(_this, item);
                        });
                    }
                })
            }

        }

        public markerClicked(marker, item) {
            $.get("http://local.test1.cz/homepage/wikipedia-text?pageId=" + item.wikipedia,
                {}, (data, status, xhr) => {
                    var modal = $("#wikiInfo");
                    $("#wikiInfoLabel", modal).text(item.name);
                    $(".modal-body", modal).html(data.data);
                    modal.modal();
                });
        }



        public createMarker(options) {
            options.map = this.map;
            return new google.maps.Marker(options);
        }


    }

}