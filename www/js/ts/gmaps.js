var Mapping;
(function (Mapping) {
    var BasicMap = (function () {
        function BasicMap(mapElement, options) {
            this.mapElement = mapElement;
            this.options = options;
            var _this = this;
            this.markers = [];
            this.paths = [];
            this.loaded = false;
            $(window).load(function () {
                _this.initialize();
            });
        }
        BasicMap.prototype.initialize = function () {
            var mapOptions = {
                zoom: this.options.zoom,
                center: new google.maps.LatLng(this.options.center.lat, this.options.center.lng),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDoubleClickZoom: true,
                streetViewControl: false
            };
            this.map = new google.maps.Map(this.mapElement, mapOptions);
            this.map.setTilt(0);
            this.loaded = true;
        };
        BasicMap.prototype.loadVenues = function (venues) {
            var _this = this;
            if(venues) {
                $.each(venues, function (index, item) {
                    var marker = _this.createMarker({
                        position: new google.maps.LatLng(item.location.lat, item.location.lng),
                        title: item.name,
                        icon: (item.wikipedia == null ? '/img/red.png' : undefined)
                    });
                    _this.markers.push(marker);
                    if(item.wikipedia != null) {
                        google.maps.event.addListener(marker, "click", function () {
                            _this.markerClicked(_this, item);
                        });
                    }
                });
            }
        };
        BasicMap.prototype.markerClicked = function (marker, item) {
            $.get("http://local.test1.cz/homepage/wikipedia-text?pageId=" + item.wikipedia, {
            }, function (data, status, xhr) {
                var modal = $("#wikiInfo");
                $("#wikiInfoLabel", modal).text(item.name);
                $(".modal-body", modal).html(data.data);
                modal.modal();
            });
        };
        BasicMap.prototype.createMarker = function (options) {
            options.map = this.map;
            return new google.maps.Marker(options);
        };
        return BasicMap;
    })();
    Mapping.BasicMap = BasicMap;    
})(Mapping || (Mapping = {}));
//@ sourceMappingURL=gmaps.js.map
