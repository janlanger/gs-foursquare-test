$(document).ready(function() {

    var map = new Mapping.BasicMap(document.getElementById("map_canvas"),{
        zoom: 8,
        center: {
            lat: 50.127622,
            lng: 14.544182
        }
    });

    $.get("/homepage/venue-data", {}, function (data, status) {
        if (status == "success") {
            map.loadVenues(data.data);
        }
    });

    $('.datepicker').datepicker({
        format: 'dd.mm.yyyy',
        weekStart: 1
    });
});