<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Jan
 * Date: 26.6.13
 * Time: 10:14
 * To change this template use File | Settings | File Templates.
 */
namespace Foursquare;


use Nette\Application\UI\Control;

class ConnectForm extends Control {

    private $foursquareApi;

    function __construct(Foursquare $foursquareApi) {
        $this->foursquareApi = $foursquareApi;
    }

    public function render() {
        $template = $this->createTemplate();
        $template->setFile(__DIR__ .'/templates/connectForm.latte');
        $template->render();
    }


    public function handleConnect() {
        $foursquare = $this->foursquareApi;

        $this->presenter->redirectUrl($foursquare->getRedirectUrl($this->link("//authorize!")));
        $this->presenter->terminate();
    }

    public function handleAuthorize() {
        $code = $this->getPresenter()->getParameter('code');
        $foursquare = $this->foursquareApi;
        $token = $foursquare->getAccessToken($code, $this->link("//authorize!"));
        if($token != NULL) {
            $userData = $foursquare->getUserData($token);
            if($userData != null) {
                $this->getPresenter()->getUser()->login($userData, $token);
                $this->getPresenter()->redirect("Homepage:default");
            }
        }
        else {
            $this->flashMessage("Propojení s foursquare se nezdařilo.", \BasePresenter::FLASH_ERROR);
        }

    }


}