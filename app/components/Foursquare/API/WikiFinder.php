<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Jan
 * Date: 26.6.13
 * Time: 13:17
 * To change this template use File | Settings | File Templates.
 */

namespace Foursquare;


use Kdyby\Curl\CurlException;
use Kdyby\Curl\Request;
use Model\Venue;
use Nette\Object;

class WikiFinder extends Object {

    private $queryUrl = "http://cs.wikipedia.org/w/api.php?action=query&prop=info&format=json&&redirects";
    private $wikiPage = "http://cs.wikipedia.org/w/index.php?action=render&curid=";
    /**
     * @param Venue[] $venues
     */
    public function findVenues($venues) {
        foreach ($venues as $venue) {
            $wikiInfo = $this->findByName($venue->getName());
            if (!empty($wikiInfo)) {
                $entry = array_shift($wikiInfo);
                if (!isset($entry['missing'])) {
                    $venue->setWikipediaPage($entry['pageid']);
                }
            }
        }
        return $venues;
    }

    public function getWikipediaInfo($pageid) {
        $request = new Request($this->wikiPage.$pageid);
        try{
            $response = $request->get();
            return $response->getResponse();
        } catch(CurlException $e) {
        }
    }

    private function findByName($name) {
        $request = new Request($this->queryUrl.'&titles='.urlencode($name));
        try{
            $response = $request->get();
            $data = json_decode($response->getResponse(), TRUE);
            return $data['query']['pages'];
        } catch(CurlException $e) {

        }
    }

}