<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Jan
 * Date: 26.6.13
 * Time: 9:35
 * To change this template use File | Settings | File Templates.
 */

namespace Foursquare;


use Kdyby\Curl\CurlException;
use Kdyby\Curl\Request;
use Model\Venue;
use Nette\Http\Url;
use Nette\Object;

class Foursquare extends Object {

    private $clientId;
    private $apiSecret;

    private $requestUrl = "https://foursquare.com/oauth2/authenticate?response_type=code";
    private $authUrl = "https://foursquare.com/oauth2/access_token";
    private $userUrl = "https://api.foursquare.com/v2/users/self";

    function __construct($clientId, $apiSecret) {
        $this->clientId = $clientId;
        $this->apiSecret = $apiSecret;
    }

    public function getRedirectUrl($returnUrl) {
        return $this->requestUrl."&client_id=" . $this->clientId."&redirect_uri=".urlencode($returnUrl);
    }

    public function getAccessToken($code, $redirectUrl) {
        $url = $this->authUrl."?code=".$code."&client_id=".$this->clientId."&client_secret=".$this->apiSecret.
                '&grant_type=authorization_code&redirect_uri='.$redirectUrl;
        $request = $this->createCurlRequest($url);
        try{
            $data = $request->get();
            $data = json_decode($data->getResponse());
            return isset($data->access_token)? $data->access_token:NULL;
        }
        catch (CurlException $e) {
            return null;
        }
    }

    public function getUserData($token) {
        $request = $this->createCurlRequest($this->userUrl);
        try {
            $data = $request->get(['oauth_token'=>$token]);
            $data = json_decode($data->getResponse());
            if(isset($data->response)) {
                return $data->response;
            }
        } catch (CurlException $e) {

        }
        return NULL;
    }

    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @param $user
     * @return Venue[]
     */
    public function loadCheckinVenues(\DateTime $from, \DateTime $to, $user) {
        $request = $this->createCurlRequest("https://api.foursquare.com/v2/users/self/checkins");
        try{
            $response = $request->get([
                'oauth_token' => $user->identity->token,
                'beforeTimestamp' => $to->getTimestamp(),
                'afterTimestamp' => $from->getTimestamp(),
                'limit' => 250,
              //  'offset' => 250
            ]);
            $data = json_decode($response->getResponse());

            $checkins = $data->response->checkins->items;

            $venues = [];

            foreach($checkins as $check) {
                $venue = $check->venue;
                if(!isset($venues[$venue->id])) {
                    $venues[$venue->id] = new Venue($venue);
                }
            }
            return $venues;

        } catch(CurlException $e) {

        }
        return [];
    }

    private function createCurlRequest($url) {
        $r = new Request($url);
        $r->setCertificationVerify(FALSE);
        return $r;
    }


}