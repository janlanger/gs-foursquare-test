<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Jan
 * Date: 26.6.13
 * Time: 10:19
 * To change this template use File | Settings | File Templates.
 */

namespace Foursquare;


use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\IIdentity;

class Authenticator implements IAuthenticator {

    public function authenticate(array $data) {
        $userdata = $data[0];
        $token = $data[1];

        $userdata->token = $token;
        return new Identity(
            $userdata->user->id,
            'user',
            $userdata
        );

    }
}