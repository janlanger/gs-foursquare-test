<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Jan
 * Date: 26.6.13
 * Time: 13:01
 * To change this template use File | Settings | File Templates.
 */
namespace Model;

use Nette\Object;

class Venue extends Object implements \JsonSerializable {
    private $id;
    private $name;
    private $foursquareUrl;
    private $location;
    private $url;
    private $wikipediaPage;
    private $wikipediaTitle;

    function __construct($venue) {
        $this->id = $venue->id;
        $this->name = $venue->name;
        $this->url = isset($venue->url)?$venue->url:NULL;
        $this->foursquareUrl = $venue->canonicalUrl;
        $this->location = $venue->location;
    }

    /**
     * @param mixed $foursquareUrl
     */
    public function setFoursquareUrl($foursquareUrl) {
        $this->foursquareUrl = $foursquareUrl;
    }

    /**
     * @return mixed
     */
    public function getFoursquareUrl() {
        return $this->foursquareUrl;
    }

    /**
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location) {
        $this->location = $location;
    }

    /**
     * @return mixed
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * @param mixed $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url) {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * @param mixed $wikipediaPage
     */
    public function setWikipediaPage($wikipediaPage) {
        $this->wikipediaPage = $wikipediaPage;
    }

    /**
     * @return mixed
     */
    public function getWikipediaPage() {
        return $this->wikipediaPage;
    }

    /**
     * @param mixed $wikipediaTitle
     */
    public function setWikipediaTitle($wikipediaTitle) {
        $this->wikipediaTitle = $wikipediaTitle;
    }

    /**
     * @return mixed
     */
    public function getWikipediaTitle() {
        return $this->wikipediaTitle;
    }


    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     *
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    public function jsonSerialize() {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'foursquareUrl' => $this->foursquareUrl,
            'location' => $this->location,
            'url' => $this->url,
            'wikipedia' => $this->wikipediaPage,
        ];
    }
}