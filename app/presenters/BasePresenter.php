<?php

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    /** Flash message type - error */
    const FLASH_ERROR = 'error';
    /** Flash message type - warning */
    const FLASH_WARNING = 'warning';
    /** Flash message type - success */
    const FLASH_SUCCESS = 'success';

}
