<?php

/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{


    public function actionWikipediaText($pageId) {

        $this->payload->data = $this->context->wikiFinder->getWikipediaInfo($pageId);

        $this->terminate();
    }

    public function actionVenueData() {
        if ($this->getSession('foursquare')->checkins) {
            $this->payload->data = $this->getSession('foursquare')->checkins;
        }
        $this->terminate();
    }

    public function createComponentFoursquareConnect($name) {
        $foursquare = new \Foursquare\ConnectForm($this->context->foursquare);
        return $foursquare;
    }

    public function createComponentDateForm($name) {
        $form = new \Components\Forms\Form();
        $form->addText("from","Od")
            ->setRequired();
        $form->addText("to", "Do")
            ->setRequired();
        $form->addSubmit("ok", "Zobrazit");
        if(isset($this->getSession('form')->from)) {
            $form['from']->value = $this->getSession('form')->from;
        }
        if (isset($this->getSession('form')->to)) {
            $form['to']->value = $this->getSession('form')->to;
        }

        $form->onSuccess[] = callback($this, "onFormSubmit");
        return $form;
    }

    public function onFormSubmit(\Components\Forms\Form $form) {
        $values = $form->getValues();
        $from = new DateTime($values['from']);
        $to = new DateTime($values['to']);
        if($from > $to) {
            $form->addError("Datum 'od' musí být před datem 'do'");
            return;
        }
        $formSession = $this->getSession('form');
        $formSession->from = $from->format('j.n.Y');
        $formSession->to = $to->format('j.n.Y');

        $foursquare = $this->context->foursquare;
        $venues = $foursquare->loadCheckinVenues($from, $to, $this->getUser());
        $venues = $this->context->wikiFinder->findVenues($venues);
        $this->getSession('foursquare')->checkins = $venues;
        $this->redirect("this");
    }

}
