<?php

use Nette\Application\UI;


/**
 * Sign in/out presenters.
 */
class SignPresenter extends BasePresenter {
    public function actionOut() {
        $this->user->logout(TRUE);
        $this->session->clean();
        $this->redirect("Homepage:default");
        $this->terminate();
    }

}
